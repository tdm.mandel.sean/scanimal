import { ContactInfo } from './ContactInfo';
import { get } from '../api/contact/contact-api';
import {createServerComponentClient} from '@supabase/auth-helpers-nextjs'
import { cookies } from 'next/headers';

export const dynamic = 'force-dynamic'

export default async function Contact() {
    const initialData = await get(createServerComponentClient({cookies}));

    return <ContactInfo contactInfo={initialData}/>
};