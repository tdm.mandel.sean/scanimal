'use client'
import { Instagram, Phone, SvgIconComponent, WhatsApp, Facebook } from '@mui/icons-material';
import { Box, Typography, Stack, Card, CardActionArea } from '@mui/material';
import { ContactInfo } from '../interfaces/meatadata';
import styles from './styles';
import { useContactInfo } from '../api/contact/use-contact';
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs'

const ContactCard = ({ Icon, onClick }: { Icon: SvgIconComponent, onClick: () => void }) => (
    <Card onClick={onClick} raised sx={styles.contactCard}>
        <CardActionArea sx={styles.contactCardAction}>
            <Icon sx={styles.contactCardIcon} />
        </CardActionArea>
    </Card>
);


interface ContactInfoProps {
    contactInfo: ContactInfo
}

export function ContactInfo({ contactInfo }: ContactInfoProps) {
    const { data, isLoading } = useContactInfo(createClientComponentClient(), contactInfo);

    const openFacebook = () => {
        window.open(`https://www.facebook.com/${data.facebookPage}`);
    };

    const openInstagram = () => {
        window.open(`https://www.instagram.com/${data.instagramUser}`);
    };

    const openWhatsapp = () => {
        window.open(`https://wa.me/${data.phoneNumber}`);
    };

    const call = () => {
        window.open(`tel:${data.phoneNumber}`);
    };

    if (isLoading) {
        return <Typography>Gerbil</Typography>
    }

    return (
        <Box display='flex' flexDirection='column' alignItems='center' justifyContent='center' gap={8} sx={styles.container}>
            <Typography variant='h2' sx={styles.title}>
                {data.name}
            </Typography>
            <Stack
                display='flex'
                justifyContent='space-evenly'
                direction={{ xs: 'column', sm: 'row', md: 'row' }}
                spacing={{ xs: 6, sm: 12, md: 24 }}
                textAlign='center'
            >
                {
                    !!data.phoneNumber &&
                    <>
                        <ContactCard onClick={call} Icon={Phone} />
                        <ContactCard onClick={openWhatsapp} Icon={WhatsApp} />
                    </>
                }
                {
                    !!data.facebookPage && <ContactCard onClick={openFacebook} Icon={Facebook} />
                }
                {
                    !!data.instagramUser && <ContactCard onClick={openInstagram} Icon={Instagram} />
                }
            </Stack>
        </Box>
    );
}