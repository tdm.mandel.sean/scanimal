import { SxProps } from '@mui/system';

const container: SxProps = {
    py: 8,
};

const title: SxProps = {
    fontWeight: 400
};

const contactCard: SxProps = {
    borderRadius: 3
};

const contactCardAction: SxProps = {
    p: 3
};

const contactCardIcon: SxProps = {
    fontSize: 128,
    mb: 1
};

const styles = { container, title, contactCard, contactCardAction, contactCardIcon };

export default styles;