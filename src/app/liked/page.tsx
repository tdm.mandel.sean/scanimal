import { AnimalCard, Gender } from '@/app/components/card';
import { Avatar, Box, CardHeader, Grid, Stack, Typography } from '@mui/material';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { IAnimal } from '../interfaces/Animal';
import { createServerComponentClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";

export default async function Liked() {
    const animalsMock: IAnimal[] = [
        {
            id: '1',
            name: 'Rexi',
            type: 1,
            age: 14,
            joiningDate: new Date(),
            img: 'https://dogsbestlife.com/wp-content/uploads/2022/07/weird-behavior-scaled.jpeg',
            gender: Gender.MALE,
            breed: 'asdasd',
            isAdopted: false,
            description: 'asdfadsf'
        },
        {
            id: '2',
            name: 'mika',
            type: 1,
            age: 14,
            joiningDate: new Date(),
            img: 'https://pbs.twimg.com/profile_images/602729491916435458/hSu0UjMC_400x400.jpg',
            gender: Gender.FEMALE,
            breed: 'asdasd',
            isAdopted: false,
            description: 'asdfadsf'
        }
    ];

    return (
        <Grid container sx={{ alignItems: 'center', flexDirection: 'column' }}>
            <Grid item>
                <CardHeader avatar={
                    <Avatar sx={{ bgcolor: 'black' }}>
                        <FavoriteIcon />
                    </Avatar>
                } title={
                    <Typography variant="h3">
                        חיות שאהבתי
                    </Typography>
                } />
            </Grid>
            <Stack spacing={2}>
                {animalsMock.map((animal) => (
                    <AnimalCard animal={animal} />
                ))}
            </Stack>
            {/* <Grid item>
                <Box sx={{flexGrow: 1, margin: '0 2%'}}>
                    <Grid container spacing={2} sx={{alignItems: 'space-around'}}>
                        
                    </Grid>
                </Box>
            </Grid> */}
        </Grid>
    )
}