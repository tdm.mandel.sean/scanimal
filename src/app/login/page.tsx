'use client';

import { useState } from 'react';
import { Button, TextField, Box } from '@mui/material';
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs';
import { useRouter } from 'next/navigation';

export default function Page() {
    const supabase = createClientComponentClient();
    const [email, setEmail] = useState('');
    const router = useRouter();

    const login = async () => {
        try {
            const res = await supabase.auth.signInWithOtp({
                email: email,
                options: {
                    emailRedirectTo: `${location.origin}/auth/callback`
                }
            });
            console.log(res);
            router.refresh();
        } catch(error) {
            console.log(error);
        }
    };

    return (<Box>
        <TextField value={email} onChange={(e) => setEmail(e.target.value)}></TextField>
        <Button onClick={login}>Login!</Button>
    </Box>);
}