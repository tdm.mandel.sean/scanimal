'use client'
import { useState, useEffect, createContext, useContext, FC, PropsWithChildren } from "react";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import { IAnimal } from "@/app/interfaces/Animal";

interface IAnimalProvider {
    animals: IAnimal[];
    refecth: () => void;
}

const AnimalsContext = createContext<IAnimalProvider>({} as IAnimalProvider);

const AnimalsProvider: FC<PropsWithChildren> = ({ children }) => {
    const supabase = createClientComponentClient();

    const [animals, setAnimals] = useState<IAnimal[]>([]);

    const fetchData = async () => {
        const { data } = await supabase.from('animal').select().returns<IAnimal[]>()
        data && setAnimals((prev) => [...prev, ...data]);
    }

    const getAnimals = async () => {
        await fetchData();

        setAnimals((prevAnimals) => prevAnimals.map(animal => {
            return { ...animal, img: supabase.storage.from('animal_picture').getPublicUrl(`dog.jfif`).data.publicUrl } as IAnimal
        })
        )
    }

    useEffect(() => { getAnimals() }, [])

    return <AnimalsContext.Provider value={{ animals, refecth: getAnimals }}>
        {children}
    </AnimalsContext.Provider>
}

export default AnimalsProvider;
export const useAnimals = () => useContext(AnimalsContext)

