import { useQuery, useMutation } from '@tanstack/react-query';
import { get, update } from './contact-api';
import { ContactInfo } from '@/app/interfaces/meatadata';
import { SupabaseClient } from '@supabase/supabase-js';
import { useInvalidateQueriesOnSuccess } from '../common';

export const useContactInfo = (client: SupabaseClient, initialData: ContactInfo) => useQuery<ContactInfo, Error>({
    queryKey: ['contact-info'],
    queryFn: () => get(client),
    initialData
});

export const useUpdateContactInfo = (client: SupabaseClient) => {
  const onSuccess = useInvalidateQueriesOnSuccess(['contact-info']);

  return useMutation(['contact-info', 'update'], (contactInfo: ContactInfo) => update(client, contactInfo), {onSuccess});
};