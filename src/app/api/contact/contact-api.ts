import { ContactInfo, MetaData } from '@/app/interfaces/meatadata'
import { SupabaseClient } from '@supabase/supabase-js';

const convertToContactInfo = (metadata: MetaData): ContactInfo => ({
    name: metadata.name,
    phoneNumber: metadata.phone_number,
    instagramUser: metadata.instagram_user,
    facebookPage: metadata.facebook_page
});

const ConvertToMetaData = (contactInfo: ContactInfo): MetaData => ({
    name: contactInfo.name,
    phone_number: contactInfo.phoneNumber ?? "",
    facebook_page: contactInfo.facebookPage ?? "",
    instagram_user: contactInfo.instagramUser ?? ""
});

const tableName = 'metadata';

export const get = async (client: SupabaseClient) => {
    const response = await client.from(tableName).select();

    console.log('response', response)

    let metadata: ContactInfo = {
        name: ''
    };

    if (response.data) {
        metadata = convertToContactInfo(response.data[0]);
    }

    return metadata;
};

export const update = async (client: SupabaseClient, newInfo: ContactInfo) => {
    await client.from(tableName).update(ConvertToMetaData(newInfo)).eq('id', 1);
};