import { useQuery, useMutation } from '@tanstack/react-query';
import { getAll, insert, update, remove } from './animal-api';
import { SupabaseClient } from '@supabase/supabase-js';
import { useInvalidateQueriesOnSuccess } from '../common';
import { IAnimal } from '@/app/interfaces/Animal';

export const useAnimal = (client: SupabaseClient, initialData: IAnimal[]) => useQuery<IAnimal[], Error>({
    queryKey: ['animal'],
    queryFn: () => getAll(client),
    initialData
});

export const useUpdateAnimal = (client: SupabaseClient) => {
  const onSuccess = useInvalidateQueriesOnSuccess(['animal']);

  return useMutation(['animal', 'update'], (animal: IAnimal) => update(client, animal), {onSuccess});
};

export const useInsertAnimal = (client: SupabaseClient) => {
  const onSuccess = useInvalidateQueriesOnSuccess(['animal']);

  return useMutation(['animal', 'insert'], (animal: IAnimal) => insert(client, animal), {onSuccess});
};

export const useRemoveAnimal = (client: SupabaseClient) => {
  const onSuccess = useInvalidateQueriesOnSuccess(['animal']);

  return useMutation(['animal', 'remove'], (id: string) => remove(client, id), {onSuccess});
};