import { SupabaseClient } from '@supabase/supabase-js';
import { IAnimal, IAnimalDto } from "@/app/interfaces/Animal";

const convertToAnimal = (animal: IAnimalDto): IAnimal => ({
  age: animal.age,
  breed: animal.breed,
  gender: animal.gender,
  id: animal.id,
  isAdopted: animal.is_adopted,
  name: animal.name,
  description: animal.description,
  joiningDate: animal.joining_date,
  type: animal.type
});

const convertToDto = (animal: IAnimal): IAnimalDto => ({
  age: animal.age,
  breed: animal.breed,
  gender: animal.gender,
  id: animal.id,
  is_adopted: animal.isAdopted,
  name: animal.name,
  description: animal.description,
  joining_date: animal.joiningDate,
  type: animal.type
});

const animalTable = 'animal';

export const getAll = async (supabaseClient: SupabaseClient) : Promise<IAnimal[]> => {
  const { data } = await supabaseClient.from(animalTable).select().returns<IAnimalDto[]>();
  
  let animals: IAnimal[] = [];

  if (data) {
    animals = data!.map(x => convertToAnimal(x)).map(animal => 
      ({...animal, 
        img: supabaseClient.storage.from('animal_picture').getPublicUrl(`${animal.id}/main.webp`).data.publicUrl
      }));
  }

  return animals;
};

export const getOne = async (supabaseClient: SupabaseClient, animalId: string) : Promise<IAnimal> => {
  const { data } = await supabaseClient.from(animalTable).select().eq("id", animalId).returns<IAnimalDto>();

  let animal: IAnimal;

  if (data) {
    animal = {...convertToAnimal(data),
          img: supabaseClient.storage.from('animal_picture').getPublicUrl(`${data.id}/main.webp`).data.publicUrl
        };
  }

  return animal;
};

export const insert = async (supabaseClient: SupabaseClient, animal: IAnimal) : Promise<IAnimal> => {
  const { data } = await supabaseClient.from(animalTable).insert(convertToDto(animal)).select();

  return convertToAnimal(data![0])
};

export const update = async (supabaseClient: SupabaseClient, animal: IAnimal) : Promise<IAnimal> => {
  const { data } = await supabaseClient.from(animalTable).update(convertToDto(animal)).eq("id", animal.id).select();

  return convertToAnimal(data![0]);
};

export const remove = async (supabaseClient: SupabaseClient, id: String) : Promise<void> => {
  await supabaseClient.from(animalTable).delete().eq("id", id);
};