'use client'

import { Box, Button, Card, Stack, Step, StepIconProps, StepLabel, Stepper, Typography, backdropClasses, styled } from "@mui/material";
import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import { QontoConnector } from "./QontoConnector";
import { Check } from "@mui/icons-material";
import { QontoStepIconRoot } from "./QontoStepIconRoot";
import { useEffect, useState } from "react";
import AnimalDetails from "./components/animalDetails";
import AnimalPersonality from "./components/animalPersonailty";
import PictureSelection from "./components/pictureSelection";
import TaskAltIcon from '@mui/icons-material/TaskAlt';

const QontoStepIcon = (props: StepIconProps) => {
  const { active, completed, className } = props;

  return (
    <QontoStepIconRoot ownerState={{ active }} className={className}>
      {completed ? (
        <Check className="QontoStepIcon-completedIcon" />
      ) : (
        <div className="QontoStepIcon-circle" />
      )}
    </QontoStepIconRoot>
  );
}

const admin = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [isDone, setIsDone] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [animalType, setAnimalType] = useState(0);
  const [firstTrait, setFirstTrait] = useState('');
  const [secondTrait, setSecondTrait] = useState('');
  const [thirdTrait, setThirdTrait] = useState('');
  const [images, setImages] = useState<any>([]);

  const steps = [
    {label: 'פרטים יבשים', form: <AnimalDetails setName={setName} setDescription={setDescription} setAnimalType={setAnimalType}/>},
    {label: 'אישיות', form: <AnimalPersonality setFirstTrait={setFirstTrait} setSecondTrait={setSecondTrait} setThirdTrait={setThirdTrait}/>},
    {label: 'תמונות', form: <PictureSelection setImages={setImages} />}
  ];

  const advanceStep = () => {
    if(activeStep === 2) {
      setIsDone(true);
      return;
    }

    setActiveStep(activeStep+1);
  }

  return <Box sx={{
      direction: 'ltr',
      height: '100vh',
      width: '100vw',
      background: 'linear-gradient(#827397, #FFFFFF)',
      display: 'flex',
      justifyContent: 'center'
    }}>
      <Stack spacing={1} sx={{ alignItems: 'center', justifyContent: 'center' }}>
      <img
              src="/long-logo.png"
              alt="Vercel Logo"
              width={200}
              height={50}
      />
      <Stack height='400px' width='300px' p='20px' spacing={3} sx={{backgroundColor: '#827397' ,alignItems: 'center', alignSelf: 'center', borderRadius: '20px', marginLeft: '100px' }}>
        {!isDone && steps[activeStep].form}
        {!isDone && <Stack direction='row' spacing={1}>
        <Button disabled={activeStep === 0} onClick={() => setActiveStep(activeStep-1)} variant='outlined' sx={{color: '#FFA800', borderColor: '#FFA800', '&:hover': {color: '#ffce70', borderColor: '#ffce70'}}}><Typography>הקודם</Typography></Button>
          <Button onClick={advanceStep} variant='contained' sx={{bgcolor: '#FFA800', '&:hover': {bgcolor: '#ffce70'}}}>
            <Typography>{activeStep === 2 ? 'סיום' : 'הבא'}</Typography>
          </Button>
        </Stack> }
        {isDone && <TaskAltIcon sx={{color: '#FFA800', width: '100px', height: '100px', marginTop: '100px !important'}} />}
        {isDone && <Typography color='#FFA800'>החייה נוספה בהצלחה למאגר האימוץ!</Typography>}
      </Stack>
      <Stepper sx={{ width: '100%'}} alternativeLabel  activeStep={activeStep} connector={<QontoConnector />}>
      {steps.map((step) => (
          <Step key={step.label}>
            <Stack spacing={2}>
            <StepLabel StepIconComponent={QontoStepIcon}>{step.label}</StepLabel>
            </Stack>
          </Step>
        ))}
      </Stepper>
      </Stack>
  </Box>
};

export default admin;