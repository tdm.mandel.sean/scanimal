import { MenuItem, Select, Stack, TextField, Typography } from "@mui/material";
import { Dispatch, SetStateAction } from "react";

interface AnimalDetailsProps {
  setName: Dispatch<SetStateAction<string>>;
  setDescription: Dispatch<SetStateAction<string>>;
  setAnimalType: Dispatch<SetStateAction<number>>;
}

const mappedAnimals = [
  {value: 0, text: 'כלב'},
  {value: 1, text: 'חתול'},
  {value: 2, text: 'זברה'},
  {value: 3, text: 'חמוסניור אופקשראל'},
]

const AnimalDetails = ({setName, setDescription, setAnimalType}: AnimalDetailsProps) => {

  return <Stack spacing={5} sx={{alignItems: 'center'}}>
    <Typography sx={{ direction: 'rtl' }}>פרטים יבשים</Typography>
    <TextField dir='rtl' label='שם' onChange={(e) => setName(e.target.value)}></TextField>
    <TextField dir='rtl' label='תיאור' onChange={(e) => setDescription(e.target.value)}></TextField>
    <Typography p='0' m='5px !important'>מה סוג החיה?</Typography>
    <Select defaultValue={0} onChange={(e) => setAnimalType(e.target.value as number)} sx={{margin: '0 !important'}} label='סוג חיה'>
      {mappedAnimals.map(animal => <MenuItem key={animal.text} value={animal.value}>{animal.text}</MenuItem>)}
    </Select>
  </Stack>
}

export default AnimalDetails;