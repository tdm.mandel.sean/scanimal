import { InputLabel, MenuItem, Select, Stack, TextField, Typography } from "@mui/material";
import { Dispatch, SetStateAction } from "react";

interface AnimalPersonalityProps {
  setFirstTrait: Dispatch<SetStateAction<string>>;
  setSecondTrait: Dispatch<SetStateAction<string>>;
  setThirdTrait: Dispatch<SetStateAction<string>>;
}

const mappedAnimalTraits = [
  {value: 0, text: 'חברותי'},
  {value: 1, text: 'חכם'},
  {value: 2, text: 'אנרגטי'},
  {value: 3, text: 'נקי'},
  {value: 4, text: 'מבולגן'},
  {value: 5, text: 'נאמן'},
]

const AnimalPersonality = ({setFirstTrait, setSecondTrait, setThirdTrait}: AnimalPersonalityProps) => {

  return <Stack spacing={0.9} sx={{alignItems: 'center'}}>
    <Typography>אישיות</Typography>
    <InputLabel shrink htmlFor='trait1'>תכונה 1</InputLabel>
    <Select onChange={(e) => setFirstTrait(e.target.name)} inputProps={{id: "trait1"}} defaultValue={0}>
      {mappedAnimalTraits.map(animal => <MenuItem key={animal.text + '1'} value={animal.value}>{animal.text}</MenuItem>)}
    </Select>
    <InputLabel shrink htmlFor='trait2'>תכונה 2</InputLabel>
    <Select onChange={(e) => setSecondTrait(e.target.name)} inputProps={{id: "trait2"}} defaultValue={2}>
      {mappedAnimalTraits.map(animal => <MenuItem key={animal.text + '2'} value={animal.value}>{animal.text}</MenuItem>)}
    </Select>
    <InputLabel shrink htmlFor='trait3'>תכונה 3</InputLabel>
    <Select onChange={(e) => setThirdTrait(e.target.name)} inputProps={{id: "trait3"}} defaultValue={5}>
      {mappedAnimalTraits.map(animal => <MenuItem key={animal.text + '3'} value={animal.value}>{animal.text}</MenuItem>)}
    </Select>
  </Stack>
}

export default AnimalPersonality;