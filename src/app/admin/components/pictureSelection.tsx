import { Button, Stack, Typography } from "@mui/material"
import { Dispatch, SetStateAction } from "react";

interface PictureSelectionProps {
  setImages: Dispatch<SetStateAction<any>>;
}

const PictureSelection = ({setImages}: PictureSelectionProps) => {

  return <Stack height='100%' sx={{alignItems: 'center', justifyContent: 'space-around'}}> 
    <Typography>העלאת תמונות</Typography>
    <input
      accept="image/webp"
      style={{ display: 'none' }}
      id="raised-button-file"
      multiple
      type="file"
      onChange={(e) => {
        if(!e.target.files){
          return;
        }

        setImages(e.target.files!);
        console.log(e.target.files)
      } }
    />
      <label htmlFor="raised-button-file">
      <Button component="span" variant='outlined' sx={{color: '#FFA800', borderColor: '#FFA800', '&:hover': {color: '#ffce70', borderColor: '#ffce70'}}}>
        <Typography>בחירה</Typography>
      </Button>
      </label> 
  </Stack>
}

export default PictureSelection;