import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import ThemeRegistry from './ThemeRegistry'
import React from 'react'
import { Box } from '@mui/material'
import Navbar from './components/Navbar';
import QueryProvider from './QueryProvider';
import AnimalsProvider from './hooks/useAnimals'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Scanimal',
  description: 'החברים הבאים שלך',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="he" dir='rtl'>
      <body className={inter.className}>
        <QueryProvider>
        <ThemeRegistry>
          <AnimalsProvider>
            <Box sx={{ width: '100vw', height: '100vh', display: 'flex', flexDirection: 'column', }}>
              <Box sx={{ display: 'flex', flex: 1 }}>
                {children}
              </Box>
              <Navbar />
            </Box>
          </AnimalsProvider>
          </ThemeRegistry>
        </QueryProvider>
      </body>
    </html>
  );
};