import { FC } from 'react';
import { Avatar, Card, CardContent, CardMedia, Grid, Stack, Typography } from '@mui/material';
import MaleIcon from '@mui/icons-material/Male';
import FemaleIcon from '@mui/icons-material/Female';
import { blue, pink } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import { IAnimal } from '@/app/interfaces/Animal';
import React from 'react';

export enum Gender {
    MALE,
    FEMALE
}

export interface AnimalCardProps {
    animal: IAnimal;
    cardHeight?: number;
    cardWidth?: number;
}

export const AnimalCard: FC<AnimalCardProps> = ({ animal: { name, img, gender }, cardHeight = 300, cardWidth = 200 }) => (
    <Card sx={{ borderRadius: '16px', width: cardWidth, height: cardHeight }}>
        <CardMedia
            sx={{ height: 150 }}
            image={img}
            title={name}
        >
            <Grid container sx={{ justifyContent: 'end' }}>
                <Avatar sx={{ bgcolor: 'white', width: 35, height: 35, margin: '3% 3%' }}>
                    {true ? <FavoriteIcon color={'error'} /> : <FavoriteBorderIcon color={'error'} />}
                </Avatar>
            </Grid>
        </CardMedia>
        <CardContent>
            <Stack spacing={1} justifyContent='center' alignItems='center'>
                <Typography gutterBottom sx={{ fontSize: '1.5em' }}>
                    {name}
                </Typography>
                <Avatar sx={{ bgcolor: gender === Gender.MALE ? blue[100] : pink[100], width: 35, height: 35 }}>
                    {gender === Gender.MALE ? <MaleIcon color={'info'} /> : <FemaleIcon color={'error'} />}
                </Avatar>
            </Stack>
        </CardContent>
    </Card>
);
