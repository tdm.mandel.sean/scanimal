import { FC } from "react";
import { Box, Typography, Icon } from '@mui/material';
import { LocationOn } from '@mui/icons-material'

interface IAdoptionDayProps {
    isHappening: true
}

const AdoptionDay: FC<IAdoptionDayProps> = ({ isHappening }) => <Box
    sx={{
        backgroundColor: '#827397',
        width: '75%',
        height: '150px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        color: 'white',
        marginTop: '30px'
    }}>
    <Typography sx={{ fontSize: '20px', fontWeight: 'bold' }}>ימי האימוץ שלנו</Typography>
    <Typography sx={{ fontSize: '20px' }}>כל יום שבת בשעות 11:00-14:00</Typography>
    <Box sx={{ display: 'flex', flexDirection: 'row' }}>
        <Typography sx={{ fontSize: '15px', paddingLeft: '5px' }}>{`יום האימוץ הקרוב:`}</Typography>
        {isHappening ?
            <Typography sx={{ fontSize: '15px', color: 'green' }}>מתקיים</Typography> :
            <Typography sx={{ fontSize: '15px', color: 'red' }}>לא מתקיים</Typography>}
    </Box>
    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', color: 'white', }}>
        <Icon sx={{ color: '#FFA800', verticalAlign: 'middle' }}><LocationOn /></Icon>
        <Typography sx={{ fontSize: '12px', }}>האימוץ של ראשון אוהבת חיות על יד ספורט-כיף</Typography>
    </Box>
</Box>

export default AdoptionDay; 