import { FC } from 'react';
import { Box } from '@mui/material'

const ScanMe: FC = () =>
    <Box
        sx={{
            backgroundColor: '#827397', width: '80%', height: '100px', borderRadius: '20px',
            display: 'flex', mt: 4, alignItems: 'center', justifyContent: 'center', fontSize: '30px'
        }}>
        Scanimal
    </Box>

export default ScanMe;