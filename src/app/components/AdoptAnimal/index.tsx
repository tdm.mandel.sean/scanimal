'use-client'
import { Box, Button, Typography } from "@mui/material";
import HorizontalScrollCards from "./HorizontalScroll";
import { getAll } from "@/app/api/animal/animal-api";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Link from 'next/link';

const AdoptAnimal = async () => {
    const initialData = await getAll(createClientComponentClient());

    return <Box sx={
        {
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            marginTop: '40px'
        }}>
        <Box sx={{
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            paddingX: '10px'
        }}>
            <Typography sx={{ fontWeight: 'bold', fontSize: '20px' }}>אמץ חיה</Typography>
            <Button variant="text" sx={{ alignSelf: 'center', color: 'black' }}>
                <Link style={{ textDecoration: "none", color: "black" }} href="/searchAnimals">לכל האופציות</Link></Button>
        </Box>
        <HorizontalScrollCards animals={initialData} />
    </Box>
}

export default AdoptAnimal;