'use client'
import React, { FC } from 'react';
import { Box, Stack } from '@mui/material';
import { AnimalCard } from '@/app/components/card';
import { useAnimals } from '@/app/hooks/useAnimals';
import { IAnimal } from '@/app/interfaces/Animal';
import Link from 'next/link';

interface IHorizontalScrollCardsProps {
    animals: IAnimal[]
}

const HorizontalScrollCards: FC<IHorizontalScrollCardsProps> = ({ animals }) => {
    return (
        <Box sx={{
            overflowX: 'auto',
            display: 'flex',
            flexDirection: 'row',
            p: 2
        }}>
            {animals?.map((animal) => (
                <Stack key={animal.id} direction='row'>
                    <Link href={`friend/${animal.id}`} key={animal.name} style={{ marginRight: '20px', textDecoration: "none", color: "black" }}>
                        <AnimalCard animal={animal} />
                    </Link>
                </Stack>
            ))
            }
        </Box>
    )
}


export default HorizontalScrollCards;
