import {
    Box, Typography, Icon
} from '@mui/material';
import { AccountCircle, FavoriteBorder, Home } from '@mui/icons-material'
import Link from 'next/link'

const Navbar = () => <Box
    sx={{
        position: 'fixed',
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        width: '100%',
        height: '80px',
        direction: 'rtl',
        paddingX: '10px',
        paddingTop: '5px',
        display: 'flex',
        zIndex: 999
    }}>
    <Link style={{ textDecoration: "none", color: "black", display: 'flex', flexDirection: 'column', alignItems: 'center' }} href='/'>
        <Icon sx={{ color: '#FFA800' }}><AccountCircle /></Icon>
        <Typography>התחבר/הרשם</Typography>
    </Link>

    <Link style={{ textDecoration: "none", color: "black", display: 'flex', flexDirection: 'column', alignItems: 'center' }} href='/'>
        <Icon sx={{ color: '#FFA800' }}><Home /></Icon>
        <Typography>בית</Typography>
    </Link>

    <Link style={{ textDecoration: "none", color: "black", display: 'flex', flexDirection: 'column', alignItems: 'center' }} href='/liked'>
        <Icon sx={{ color: '#FFA800' }}><FavoriteBorder /></Icon>
        <Typography>אהבתי</Typography>
    </Link>
</Box>

export default Navbar;