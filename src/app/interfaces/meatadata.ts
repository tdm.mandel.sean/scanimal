export interface MetaData {
    name: string;
    phone_number: string;
    facebook_page: string;
    instagram_user: string;
}

export interface ContactInfo {
    name: string;
    phoneNumber?: string;
    facebookPage?: string;
    instagramUser?: string;
}