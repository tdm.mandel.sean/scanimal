export interface IAnimal {
    id: string
    name: string;
    type: number;
    img?: string;
    age: number;
    joiningDate?: Date;
    gender: number;
    description?: string;
    isAdopted: boolean;
    breed: string
}

export interface IAnimalDto {
    id: string
    name: string;
    type: number;
    age: number;
    joining_date?: Date;
    gender: number;
    description?: string;
    is_adopted: boolean;
    breed: string
}