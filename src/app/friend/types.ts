export const Gender = {
    Male: 1,
    Female: 2,
    Other: 3
} as const;

export const genderTranslator = {
    [Gender.Male]: 'זכר',
    [Gender.Female]: 'נקבה',
    [Gender.Other]: 'אחר'
} as const;

export const AnimalType = {
    Dog: 1,
    Cat: 2
} as const;

export const animalTypeTranslator = {
    [AnimalType.Cat]: 'חתול ',
    [AnimalType.Dog]: 'כלב'
} as const;

export interface Animal {
    id: string;
    name: string;
    age: number;
    type: number;
    joiningDate: string;
    description: string;
    gender: number;
    breed: string;
    isAdopted: boolean;
}