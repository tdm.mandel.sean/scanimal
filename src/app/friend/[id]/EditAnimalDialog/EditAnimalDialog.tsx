import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, Stack, TextField, Typography } from '@mui/material';
import { MouseEventHandler, useState, useEffect, ChangeEvent } from 'react';
import { Animal, AnimalType, animalTypeTranslator, Gender, genderTranslator } from '../../types';
import { IAnimal } from '@/app/interfaces/Animal';
import { useUpdateAnimal } from '@/app/api/animal/use-animal';
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs';

interface EditAnimalDialogProps {
    animal: IAnimal;
    isOpen: boolean;
    onClose: MouseEventHandler<HTMLButtonElement>;
}

const EditAnimalDialog = ({ animal, isOpen, onClose }: EditAnimalDialogProps) => {
    const [duplicateAnimal, setDuplicateAnimal] = useState<IAnimal>(animal);
    const {mutate} = useUpdateAnimal(createClientComponentClient());

    useEffect(() => {
        setDuplicateAnimal(animal);
    }, [isOpen]);

    const saveEdit = (event) => {
        mutate(duplicateAnimal);
        onClose(event);
    };

    const onNameChange = ({target: {value}}: ChangeEvent<HTMLInputElement>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            name: value
        }))
    };

    const onAgeChange = ({target: {value}}: ChangeEvent<HTMLInputElement>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            age: value as unknown as number
        }))
    };

    const onTypeChange = ({target: {value}}: SelectChangeEvent<number>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            type: value as number
        }))
    };

    const onGenderChange = ({target: {value}}: SelectChangeEvent<number>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            gender: value  as number
        }))
    };

    const onBreedChange = ({target: {value}}: ChangeEvent<HTMLInputElement>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            breed: value
        }))
    };

    const onDescriptionChange = ({target: {value}}: ChangeEvent<HTMLInputElement>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            description: value
        }))
    };

    const toggleIsAddopted = ({target: {checked}}: ChangeEvent<HTMLInputElement>) => {
        setDuplicateAnimal(animal => ({
            ...animal,
            isAdopted: checked
        }))
    };

    return (
        <Dialog open={isOpen} onClose={onClose} fullWidth>
            <DialogTitle>עריכה</DialogTitle>
            <DialogContent>
                <Stack spacing={2}>
                    <TextField
                        id="name"
                        label="שם"
                        fullWidth
                        variant="standard"
                        value={duplicateAnimal.name}
                        onChange={onNameChange}
                    />
                    <TextField
                        id="age"
                        label="גיל"
                        fullWidth
                        variant="standard"
                        type="number"
                        value={duplicateAnimal.age}
                        onChange={onAgeChange}
                    />
                    <FormControl>
                        <InputLabel id="type">סוג</InputLabel>
                        <Select
                            id="type"
                            labelId="type"
                            fullWidth
                            variant="standard"
                            value={duplicateAnimal.type}
                            onChange={onTypeChange}
                        >
                            {
                                Object.values(AnimalType).map(x => (
                                    <MenuItem key={x} value={x}>{animalTypeTranslator[x]}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <FormControl>
                        <InputLabel id="gender">מין</InputLabel>
                        <Select
                            id="gender"
                            label="מין"
                            fullWidth
                            variant="standard"
                            value={duplicateAnimal.gender}
                            onChange={onGenderChange}
                        >
                            {
                                Object.values(Gender).map(x => (
                                    <MenuItem key={x} value={x}>{genderTranslator[x]}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <TextField
                        id="breed"
                        label="גזע"
                        fullWidth
                        variant="standard"
                        value={duplicateAnimal.breed}
                        onChange={onBreedChange}
                    />
                    <TextField
                        id="description"
                        label="תיאור"
                        fullWidth
                        variant="standard"
                        multiline
                        minRows={3}
                        value={duplicateAnimal.description}
                        onChange={onDescriptionChange}
                    />
                    <Stack direction='row' spacing={1} alignItems='center'>
                        <Checkbox checked={duplicateAnimal.isAdopted} onChange={toggleIsAddopted}/>
                        <Typography>אומץ?</Typography>
                    </Stack>
                </Stack>
            </DialogContent>
            <DialogActions>
                <Button onClick={saveEdit}>שמור</Button>
                <Button onClick={onClose}>ביטול</Button>
            </DialogActions>
        </Dialog>
    )
};

export default EditAnimalDialog;