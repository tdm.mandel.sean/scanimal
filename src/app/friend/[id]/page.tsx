'use client'
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import React, { useState } from 'react';
import Typography from '@mui/material/Typography';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import FavoriteIcon from '@mui/icons-material/Favorite';
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt';
import Carousel from 'react-material-ui-carousel';
import { Button, IconButton } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import { usePathname } from 'next/navigation';
import QRCode from 'qrcode';
import DeleteAnimalDialog from './DeleteAnimalDialog';
import EditAnimalDialog from './EditAnimalDialog';
import Trait from './Trait';
import { Animal, AnimalType, Gender, animalTypeTranslator, genderTranslator } from '../types';
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs';
import { useAnimal } from '../../api/animal/use-animal';

interface PageProps {
  params: {
    id: string;
  };
};

export default function Page({ params: { id } }: PageProps) {
  const {data, isLoading} = useAnimal(createClientComponentClient(), []);
  const pathname = usePathname();
  const [isEditDialogOpen, setIsEditDialogOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const isLiked = true;
  const isAdmin = true;

  const openEditAnimalDialog = () => {
    setIsEditDialogOpen(true);
  };

  const closeEditAnimalDialog = () => {
    setIsEditDialogOpen(false);
  };

  const openDeleteAnimalDialog = () => {
    setIsDeleteDialogOpen(true);
  };

  const closeDeleteAnimalDialog = () => {
    setIsDeleteDialogOpen(false);
  };

  const exportQR = async () => {
    const qrUri = await QRCode.toDataURL(`${location.origin}${pathname}`)

    const anchor = document.createElement('a');
    anchor.href = qrUri;
    anchor.download = `${animal.name}QR.png`;
    anchor.click();

    URL.revokeObjectURL(qrUri);
  };

  if (isLoading) {
    return <Typography>בטעינה...</Typography>
  }

  const animal = data.find(x => x.id === id);

  if (!animal) {
    return <Typography>באסה</Typography>
  }


  return (
    <>
      <Stack sx={{ height: 1, width: 1 }}>
        <Box flex={5} zIndex={1} sx={{ overflow: 'hidden' }}>
          <Carousel animation='slide' duration={300} indicators={false} height={500}>
            {
              [animal.img].map((image, index) => (
                <Box
                  key={index}
                  sx={{
                    backgroundImage: `url(${image})`,
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    display: 'block',
                    overflow: 'hidden',
                    height: 1,
                    width: 1,
                  }}
                />
              ))
            }
          </Carousel>
        </Box>
        <Box flex={7} zIndex={2}>
          <Paper sx={{
            height: 1,
            width: 1,
            p: 1,
            pt: 2,
            boxShadow: '0 0 0 4px rgba(100, 100, 100, 0.3)',
            background: 'linear-gradient(180deg, rgba(255,255,255,0) 60%, rgba(255,168,0,0.6) 100%)'
          }}
          >
            <Stack spacing={2}>
              <Box display='flex' justifyContent='space-between' alignItems='center' sx={{ px: 2 }}>
                <Stack direction='row' spacing={1} alignItems='center'>
                  <Typography variant='h4' sx={{ color: '#4c4a4f' }} fontWeight='bold'>{animal.name}</Typography>
                  {
                    isAdmin ?
                      <Box display='flex' flexDirection='row'>
                        <Button onClick={openEditAnimalDialog} sx={{ display: 'flex', flexDirection: 'column', color: 'secondary', p: 0 }}>
                          <EditIcon />
                          <Typography variant='caption'>ערוך</Typography>
                        </Button>
                        <Button onClick={openDeleteAnimalDialog} sx={{ display: 'flex', flexDirection: 'column', color: 'secondary', p: 0 }}>
                          <DeleteIcon />
                          <Typography variant='caption'>מחק</Typography>
                        </Button>
                        <Button onClick={exportQR} sx={{ display: 'flex', flexDirection: 'column', color: 'secondary', p: 0 }}>
                          <QrCodeScannerIcon />
                          <Typography variant='caption'>הוצא ברקוד</Typography>
                        </Button>
                      </Box>
                      : null
                  }
                </Stack>
                <IconButton sx={{ color: '#FF4646', p: 0 }}>
                  {
                    isLiked ? <FavoriteIcon fontSize='large' /> : <FavoriteBorderIcon fontSize='large' />
                  }
                </IconButton>
              </Box>
              <Stack sx={{ px: 2 }} direction='row' spacing={1} justifyContent='space-evenly'>
                <Trait title={'גיל'} description={animal.age} />
                <Trait title={'גזע'} description={animalTypeTranslator[animal.type]} />
                <Trait title={'מין'} description={genderTranslator[animal.gender]} />
              </Stack>
              <Stack sx={{ p: 2 }} spacing={2}>
                <Stack direction='row' spacing={1} alignItems='center'>
                  <Typography variant='h5' color='secondary' fontWeight='bold' fontSize='1.7rem'>הסיפור שלי</Typography>
                  <SentimentSatisfiedAltIcon fontSize='medium' color='secondary' />
                </Stack>
                <Box>
                  <Typography variant='h6'>{animal.description}</Typography>
                </Box>
              </Stack>
            </Stack>
          </Paper>
        </Box>
      </Stack>
      <EditAnimalDialog animal={animal} isOpen={isEditDialogOpen} onClose={closeEditAnimalDialog} />
      <DeleteAnimalDialog animal={animal} isOpen={isDeleteDialogOpen} onClose={closeDeleteAnimalDialog} />
    </>
  );
};