import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@mui/material';
import { MouseEventHandler } from 'react';
import { Animal } from '../../types';
import { IAnimal } from '@/app/interfaces/Animal';
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs';
import { useRemoveAnimal } from '@/app/api/animal/use-animal';

interface DeleteAnimalDialogProps {
    animal: IAnimal;
    isOpen: boolean;
    onClose: MouseEventHandler<HTMLButtonElement>;
}

const DeleteAnimalDialog = ({ animal, isOpen, onClose }: DeleteAnimalDialogProps) => {
    const {mutate} = useRemoveAnimal(createClientComponentClient());

    const onDelete = (event) => {
        mutate(animal.id);
        onClose(event);
    };

    return (
        <Dialog open={isOpen} onClose={onClose}>
            <DialogTitle>אישור מחיקה</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    האם אתה בטוח שאתה רוצה למחוק את {animal.name}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onDelete} sx={{ color: 'red' }}>מחק</Button>
                <Button onClick={onClose}>ביטול</Button>
            </DialogActions>
        </Dialog>
    )
};

export default DeleteAnimalDialog;