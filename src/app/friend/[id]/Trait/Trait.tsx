import { Paper, Typography } from '@mui/material';

interface TraitProps {
    title: string;
    description: string | number;
}

const Trait = ({ title, description }: TraitProps) => (
    <Paper sx={{ p: 1, width: '70px', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Typography variant='h6'>{title}</Typography>
        <Typography variant='h6' sx={{ fontWeight: 'bold' }} color='secondary'>{description}</Typography>
    </Paper>
);

export default Trait;