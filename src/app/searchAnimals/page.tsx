import { createServerComponentClient } from "@supabase/auth-helpers-nextjs";
import SearchBar from "./Components/SearchBar";
import { Box, Stack } from "@mui/material";
import Animals from "./Components/Animals";
import { getAll } from "@/app/api/animal/animal-api"
import { cookies } from "next/headers";

export const dynamic = 'force-dynamic'

export default async function Page() {
    const initialData = await getAll(createServerComponentClient({cookies}));

    return <Stack sx={{ width: '100%', p: 3 }} spacing={3}>
        <SearchBar />
        <Animals animals={initialData}/>
    </Stack>
}
