'use client'
import React, { useState, FC } from 'react';
import { TextField } from '@mui/material';
import { styled } from 'styled-components'

interface ISearchBarProps {
  onSearch: (searchValue: string) => void
}

export const StyledTextField = styled(TextField)({
  '& label': {
    transformOrigin: "right !important",
    left: "inherit !important",
    right: "1.75rem !important",
    fontSize: "small",
    color: "#807D7B",
    fontWeight: 400,
    overflow: "unset",
  },})

  const SearchBar: FC = () => {
  const [searchTerm, setSearchTerm] = useState<string>('');

  const handleSearchChange = (event: any) => {
    const newSearchTerm = event.target.value;
    setSearchTerm(newSearchTerm);
    // onSearch(newSearchTerm);
  };

  return (
    <StyledTextField
      label="חיפוש..."
      variant='standard'
      fullWidth
      sx={{ input: { color: '#827397' } }}
      value={searchTerm}
      onChange={handleSearchChange}
    />
  );
};

export default SearchBar;
