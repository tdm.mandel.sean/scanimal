'use client'
import { Grid, Stack, Typography } from "@mui/material";
import { AnimalCard } from "@/app/components/card";
import { useAnimal } from "@/app/api/animal/use-animal";
import { IAnimal } from "@/app/interfaces/Animal";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Link from 'next/link';
import React from 'react';

const Animals = ({ animals }: { animals: IAnimal[] }) => {

    const { data, isLoading } = useAnimal(createClientComponentClient(), animals);

    if (isLoading) {
        return <Typography>Loading...</Typography>
    }

    return (
        <Stack spacing={2} direction="row" useFlexGap flexWrap="wrap" alignItems='center' justifyContent='center'>
            {
                data.map((animal) =>
                    <Link style={{ textDecoration: "none", color: "black" }} href={`friend/${animal.id}`}>
                        <AnimalCard animal={animal} />
                    </Link>
                )
            }
        </Stack>
    )
}

export default Animals;