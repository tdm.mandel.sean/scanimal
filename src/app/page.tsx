import { Box } from "@mui/material";
import ScanMe from "./components/ScanMe";
import AdoptionDay from "./components/AdoptionDay";
import AdoptAnimal from "./components/AdoptAnimal";

export default function Home() {
  return (
    <Box sx={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
      <ScanMe />
      <AdoptionDay isHappening={true} />
      <AdoptAnimal />
    </Box>
  )
}
