export interface IAnimal {
    id: string
    name: string;
    type?: number;
    img?: string ;
    age?: number;
    joining_date?: Date;
    gender?: number;
    description?: string;
    is_adopted: boolean;
    breed: string
}
